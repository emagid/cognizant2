<div class="row">
    <div class="col-md-8">
        <div class="box">
            <div class="input-group">
                <input id="search" type="text" name="search" class="form-control" placeholder="Search by Id or Name"/>
            <span class="input-group-addon">
                <i class="icon-search"></i>
            </span>
            </div>
        </div>
    </div>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
</div>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
    $(document).ready(function () {

        $('body').on('change', '.change', function () {

            var id = $(this).attr('data-id');
            var status = $(this).val();

            $.ajax({
                type: "POST",
                url: "/admin/orders/update_post_status/",
                data: "id=" + id + "&status=" + status,
                success: function (html) {


                }
            });
        });

    });


</script>
<?php if (count($model->orders) > 0) { ?>
    <div class="box box-table">
        <table id="data-list" class="table">
            <thead>
            <tr>
                <th width="5%"></th>
                <th>Order #</th>
                <th>Date</th>
                <th>Tracking #</th>
                <th>Payment Method</th>
                <th>Status</th>
                <th>Bill Name</th>
                <th>Brands</th>
                <th>Products MPN</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->orders as $obj) { ?>
                <tr class="originalProducts">
                    <td>
                        <? if (is_null($obj->viewed) || !$obj->viewed) { ?>
                            <button type="button" class="btn btn-success" disabled="disabled">New</button>
                        <? } ?>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->id; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->insert_time; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->tracking_number; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            <?php switch($obj->payment_method){
                                case 1:
                                    $data = "Credit/Debit";
                                    break;
                                case 2:
                                    $data = "Bank Wire";
                                    break;
                                case 3:
                                    $data = "Paypal";
                                    break;
                            } echo $data; ?>
                        </a>
                    </td>
                    <td>
                        <? sort(\Model\Order::$status, SORT_LOCALE_STRING); ?>
                        <select class="change" data-id="<?php echo $obj->id; ?>">
                            <?php $current_status = $obj->status; ?>
                            <? foreach (\Model\Order::$status as $key => $stat) { ?>
                                <option <? if ($current_status == $stat) {
                                    echo "selected";
                                } ?> value="<?= $stat ?>"><?= $stat ?></option>
                            <? } ?>
                        </select>

                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            <? $shp = array($obj->ship_first_name, $obj->ship_last_name, $obj->ship_address, $obj->ship_address2,
                                $obj->ship_country, $obj->ship_city, $obj->ship_state, $obj->ship_zip);


                            $blng = array($obj->bill_first_name, $obj->bill_last_name, $obj->bill_address, $obj->bill_address2,
                                $obj->bill_country, $obj->bill_city, $obj->bill_state, $obj->bill_zip);


                            if (count(array_diff($shp, $blng)) == 0) {
                                ?><?php echo $obj->bill_first_name . ' ' . $obj->bill_last_name; ?>
                            <? } else {
                                ?>
                                <button type="button" class="btn btn-success" style="position:relative;background:red;"
                                        disabled="disabled"> <?php echo $obj->bill_first_name . ' ' . $obj->bill_last_name; ?></button>
                            <? } ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            <? foreach ($model->products[$obj->id] as $key => $product) { ?>
                                <?= $product->brand_name ?> <?= ($key < (count($model->products[$obj->id]) - 1)) ? '<br />' : '' ?>
                            <? } ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            <? $i = 0; ?>
                            <? foreach ($model->products[$obj->id] as $key => $product) { ?>
                                <? $i++; ?>
                                <?= $product->mpn ?> <?= ($key < (count($model->products[$obj->id]) - 1)) ? '<br />' : '' ?>
                            <? } ?>

                        </a>
                        <? if ($i > 1) { ?>
                            <button type="button" class="btn btn-success" style="position:relative;"
                                    disabled="disabled"><?= $i ?></button><? } ?>
                    </td>

                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            <? if ($obj->payment_method == 2) { ?>
                                <button type="button" class="btn btn-success" style="position:relative;"
                                        disabled="disabled">$<?php echo number_format($obj->total, 2); ?></button>
                            <? } else { ?>$<?php echo number_format($obj->total, 2);
                            } ?>
                        </a>
                    </td>

                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>

<?php footer(); ?>

<script>
    var site_url = '<?= ADMIN_URL.'orders';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script type="text/javascript">
    $(function () {
        $("#search").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>orders/search";
            var keywords = $(this).val();
            if (keywords.length > 0) {
                $.get(url, {keywords: keywords}, function (data) {
                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();
                    var list = JSON.parse(data);
                    for (key in list) {
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html();
                        $('<td />').appendTo(tr).html(list[key].id);
                        $('<td />').appendTo(tr).html(list[key].insert_time);
                        $('<td />').appendTo(tr).html(list[key].tracking_number);
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>orders/update/' + list[key].id).html(list[key].status));
                        $('<td />').appendTo(tr).html(list[key].bill_name);
                        $('<td />').appendTo(tr).html(list[key].products);
                        $('<td />').appendTo(tr).html(list[key].brands);
                        $('<td />').appendTo(tr).html("$" + list[key].total);
                        var editTd = $('<td />').addClass('text-center').appendTo(tr);
                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>orders/update/' + list[key].id);
                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        });
    })
</script>